#cd /var/lib/jenkins/workspace/shengtai/shengtai-admin
mvn clean package

PID=$(ps -ef | grep afeng-rpg | grep -v grep | awk '{ print $2 }')
if [ -z "$PID" ]
then
    echo Application is not found
else
    echo kill $PID
    kill $PID
fi
BUILD_ID=dontKillMe

nohup java -jar  -Xms180m -Xmx180m  target/*.jar &