package com.example.demo.enums;

public enum DaoJuType {
    MAN("1", "男"),
    WOMEN("0", "女"),
    TAIJIAN("2", "无根"),
    ;

    private String code;
    private String dsec;

    DaoJuType(String code, String dsec) {
        this.code = code;
        this.dsec = dsec;
    }

    public String getCode() {
        return code;
    }

    public String getDsec() {
        return dsec;
    }
}
