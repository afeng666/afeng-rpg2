package com.example.demo.enums;

/**
 * @description:
 * @author: liqf
 * @create: 2023-02-28 21:22
 **/
public enum SexEnum {
    MAN("1", "男"),
    WOMEN("0", "女"),
    TAIJIAN("2", "无根"),
    ;

    private String code;
    private String dsec;

    SexEnum(String code, String dsec) {
        this.code = code;
        this.dsec = dsec;
    }

    public String getCode() {
        return code;
    }

    public String getDsec() {
        return dsec;
    }
}

