package com.example.demo;

import com.alibaba.fastjson.JSON;
import com.example.demo.domain.Bag;
import com.example.demo.domain.DaoJu;
import com.example.demo.domain.role.JiLeGuPerson;
import com.example.demo.domain.wuqi.Jian;
import com.example.demo.domain.wuqi.WuQi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

@SpringBootApplication
public class AfengRpgApplication {

//    @Autowired private JdbcTemplate jdbcTemplate;
    //todo 如何交给elk处理日志--00
    public static void main(String[] args) {

//        List<Map<String, Object>> maps = new AfengRpgApplication().jdbcTemplate.queryForList("");

        SpringApplication.run(AfengRpgApplication.class, args);
    }

    @PostConstruct
    public void init(){
        System.out.println("游戏开始-->");
        JiLeGuPerson jiLeGuPerson = new JiLeGuPerson();
        jiLeGuPerson.setName("张三");
        Bag<DaoJu> bag = jiLeGuPerson.getBag();
        Map<DaoJu, Integer> map = bag.getMap();
        map.put(new Jian(){{setName("剑");}}, 1);

        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNext()) {
            String s = scanner.nextLine();
            if ("b".equals(s)) {
                //展示背包
                System.out.println(" 背包 = " + JSON.toJSONString(jiLeGuPerson.getBag().getMap()));
            }if ("c".equals(s)) {
                //任务属性
                System.out.println("人物属性 = " + JSON.toJSONString(jiLeGuPerson));
            }if ("q".equals(s)) {
                //切换武器
                System.out.println("切换武器...");
                map.forEach((k,v)->{
                    if (k instanceof WuQi) {
                        jiLeGuPerson.setCurrentHand(k);
                    }
                });
                System.out.println("人物属性 = " + JSON.toJSONString(jiLeGuPerson));
            }if ("".equals(s)) {

            }if ("".equals(s)) {

            }if ("".equals(s)) {

            }if ("".equals(s)) {

            }if ("".equals(s)) {

            }if ("".equals(s)) {

            }if ("".equals(s)) {

            }if ("".equals(s)) {

            }if ("".equals(s)) {

            }if ("".equals(s)) {

            }if ("".equals(s)) {

            }if ("".equals(s)) {

            }if ("".equals(s)) {

            }if ("".equals(s)) {

            }if ("".equals(s)) {

            }if ("".equals(s)) {

            }if ("".equals(s)) {

            }if ("".equals(s)) {

            }if ("".equals(s)) {

            }if ("".equals(s)) {

            }
        }
    }
}
