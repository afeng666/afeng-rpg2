package com.example.demo.domain;

import lombok.Getter;
import lombok.Setter;

/**
 * @description: 角色
 * @author: liqf
 * @create: 2023-02-28 21:18
 **/
@Setter
@Getter
public class Actor {
    private String name;
}

