package com.example.demo.domain.role;

import com.example.demo.domain.Actor;
import com.example.demo.domain.Bag;
import com.example.demo.domain.DaoJu;
import com.example.demo.enums.DaoJuType;
import com.example.demo.enums.SexEnum;
import lombok.Getter;
import lombok.Setter;

/**
 * @description:
 * @author: liqf
 * @create: 2023-02-28 21:18
 **/
@Setter
@Getter
public class Person extends Actor {

    private SexEnum sexEnum;

    /**
     * 健康度
     */
    private Integer health;
    /**
     * 体力
     */
    private Integer tiLi;
    /**
     * 臂力
     */
    private Integer biLi;
    /**
     * 内息
     */
    private Integer neiXi;

    /**
     * 默认有包
     */
    private Bag<DaoJu> bag = new Bag<>();

    /**
     * 手中
     */
    private DaoJu currentHand;
}

