package com.example.demo.domain;

import com.example.demo.enums.DaoJuType;
import lombok.Getter;
import lombok.Setter;

/**
 * @description: 道具
 * @author: liqf
 * @create: 2023-02-28 21:33
 **/
@Setter
@Getter
public class DaoJu extends Actor{

    /**
     * 描述
     */
    private String dsec;

    /**
     * 类型
     */
    private DaoJuType daoJuType;

}

