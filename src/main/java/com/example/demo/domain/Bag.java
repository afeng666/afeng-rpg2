package com.example.demo.domain;

import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;
import java.util.Map;

/**
 * @description: 背包
 * @author: liqf
 * @create: 2023-02-28 21:28
 **/
@Setter
@Getter
public class Bag<T extends Object> {

    /**
     * 格子大小
     */
    private Integer size;
    /**
     * 背包容器
     */
    private  Map<T ,Integer> map = new HashMap<>();

}

